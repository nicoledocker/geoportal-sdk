# DEPENDENCIES (26/05/2021)

* geoportal-extensions-openlayers@3.1.0
└── geoportal-access-lib@2.1.8

* geoportal-extensions-itowns@2.3.2
└── geoportal-access-lib@2.1.8

* "itowns"
└── "itowns@2.30.0"

* "openlayers"
└── "openlayers@6.3.1
