# SDK Geoportail 2D/3D, version 3.3.3

**10/11/2021 : version 3.3.3**

> Release SDK Geoportail 2D/3D

## Summary

* Correction couche itinéraire en doublon dans le layerSwitcher
 
## Changelog

* [Added]

* [Changed]

    - geoportal access lib 3.0.3
    - extension geoportail pour openlayers 3.2.3

* [Removed]

* [Fixed]

    - correction sur l'affichage des couches itinéraires dans le layerSwitcher

* [Deprecated]

* [Security]
